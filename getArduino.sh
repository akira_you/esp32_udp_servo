mkdir -p components && \
cd components && \
git clone https://github.com/espressif/arduino-esp32.git arduino && \
cd arduino && \
git submodule update --init --recursive && \
cd ../.. && \
make menuconfig

cd components/arduino/libraries && \
git clone https://github.com/EAIBOT/ydlidar_arduino YDLidar && \
cd  YDLidar && \
mkdir -p src && \
mv *.h *.cpp inc src


