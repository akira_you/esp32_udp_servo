/////////////////////////////////////
//task for servo 
////////////////////////////////////
#undef INADDR_NONE
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include <sys/socket.h>
#include "driver/ledc.h"
#include "ydlidarTask.h"

#include "angleTask.h"
const static char *TAG="servoTask";


#define DANGER_LEN 600

#define STACK_SIZE 3000

#define LEDC_CH0_GPIO       (26)
#define LEDC_CH1_GPIO       (27)
#define PWM_BITWIDTH 15
#define PWM_WIDTH  0x7fff //2**PWM_BITWIDTH
static portMUX_TYPE servoMutex = portMUX_INITIALIZER_UNLOCKED;
static void lockServo(){
    taskENTER_CRITICAL(&servoMutex);
}
static void unlockServo(){
    taskEXIT_CRITICAL(&servoMutex);
}

int deg2pw(float deg)
{
    float ms = ((float) deg - 90.0) * 0.95 / 90.0 + 1.45;
    return (int) (ms / 15.625* PWM_WIDTH);
}
#define  NeutralS 90
#define  NeutralA 94
float targetSpeed;
float degS=NeutralS;
ledc_channel_config_t ledc_channel[2];
xTaskHandle servoTaskHandle;
int16_t lastPos;
TickType_t lastTick; //16bit or 32bit
float speed;
float speedAve;


static struct sockaddr_in host_addr;
static int _mysocket=-1;



static char statusStr[2000];
static uint8_t udpWdtCount=0;
void servoTask(void *pvParameters)
{
    lastPos=0;
    speed=0;
    speedAve=0;
    int nowDutyA=deg2pw(NeutralA);
    int dutyMaxA=deg2pw(NeutralA+15);
    int dutyMinA=deg2pw(NeutralA-15);
    uint8_t count=0;
    lastTick=xTaskGetTickCount(); 
    while(true){
        bool danger=false;
        count++;
        vTaskDelay(50 / portTICK_PERIOD_MS);
        //copy lidar data for safty lock
        static ydlidar_summary_t ydlidar_sum;
        ydlidar_get_summary(ydlidar_sum);
        //16-31 is 180 degree front
        // danger detection 17-30
        for(int i=17;i<=30;i++){
            if( (100<ydlidar_sum[i]) && (ydlidar_sum[i]<DANGER_LEN)){
                danger=true;
                targetSpeed=0;
                //ESP_LOGI(TAG,"danger %d %d",i,ydlidar_sum[i]);
            }
        }



        lockServo();
        //////////////////////////////
        //calculate speed
        //////////////////////////////
        int16_t nowPos=getAngle();
        TickType_t nowTick=xTaskGetTickCount();
        //Rotation per second.
        //speed= (nowPos-lastPos)/256.0f  / ((float)(nowTick-lastTick)/configTICK_RATE_HZ);
        int16_t diff=nowPos-lastPos;
        int16_t diffTick= (int16_t)(nowTick-lastTick);
        if(diffTick>0){
            speed= diff*configTICK_RATE_HZ/ diffTick/256.0;
            speedAve=0.5*speed+0.5*speedAve;
        }
        lastPos=nowPos;
        lastTick=nowTick;

        ///////////////////////////////
        //set PWM duty
        ///////////////////////////////
        if(udpWdtCount==0){
            degS=NeutralS;
            //Slow stop when no signal
            int sig=1;
            if(targetSpeed>0){
                sig=-1;
                targetSpeed*=-1;
            }
            targetSpeed-=0.25;
            if(danger)targetSpeed-=0.5;//addtional break
            if(targetSpeed<0)targetSpeed=0;
            targetSpeed*=sig;
        }else{
            udpWdtCount--;
        }
        
        nowDutyA+=(targetSpeed-speedAve)*7;
        if(nowDutyA<dutyMinA)nowDutyA=dutyMinA;
        if(nowDutyA>dutyMaxA)nowDutyA=dutyMaxA;
        
        
        ledc_set_duty   (ledc_channel[0].speed_mode, ledc_channel[0].channel, deg2pw(degS));
        ledc_update_duty(ledc_channel[0].speed_mode, ledc_channel[0].channel);
        ledc_set_duty   (ledc_channel[1].speed_mode, ledc_channel[1].channel, nowDutyA);
        ledc_update_duty(ledc_channel[1].speed_mode, ledc_channel[1].channel);
        /*
        ledc_channel[0].duty= deg2pw(degS);
        ledc_channel[1].duty= nowDutyA;
        for (int ch = 0; ch <2; ch++) {
            if(ESP_OK != ledc_channel_config(&(ledc_channel[ch]))){
                ESP_LOGE(TAG,"Error on ledc_channel");
            }
        }
        */
       unlockServo();
        ///////////////////////////////////////////////////////
        // send status
        //////////////////////////////////////////////////////

        

        if(count%2==0){
            char *sts=statusStr;
            sts+=sprintf(sts,"{\"L\":[%d",ydlidar_sum[0]);
            for(int i=0;i<YDLIDAR_SUMMARY_LEN;i++){
                sts+=sprintf(sts,",%d ",(int)ydlidar_sum[i]);
            }
            sts+=sprintf(sts,"]}");
            if(count % 100==0){
                ESP_LOGI(TAG,"%s",statusStr);
            }

            ESP_LOGI(TAG,"Speed %f   nowPos:%x   duty:%d",speed,(int)nowPos,nowDutyA);
            if(_mysocket>0)
                sendto(_mysocket, statusStr, strlen(statusStr)+1, 0, (struct sockaddr *)&host_addr, sizeof(host_addr));
        }
    }
}




esp_err_t _servoTaskSetup(void){
    esp_err_t ret;
    ledc_timer_config_t ledc_timer;
    bzero(&host_addr,sizeof(host_addr));
    host_addr.sin_family = AF_INET;
    host_addr.sin_port = htons(13532);
    host_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    ledc_timer.duty_resolution = (ledc_timer_bit_t)PWM_BITWIDTH; // resolution of PWM duty
    ledc_timer.freq_hz = 64;              // frequency of PWM signal
    ledc_timer.speed_mode = LEDC_HIGH_SPEED_MODE;   // timer mode
    ledc_timer.timer_num = (ledc_timer_t)0 ;   // timer index [0-3]
    
    // Set configuration of timer0 for high speed channels
    ret=ledc_timer_config(&ledc_timer);

    if(ESP_OK!=ret){ESP_LOGE(TAG,"Error on ledc_timer_config1:%d",ret);return ret;}
    ledc_timer.timer_num=(ledc_timer_t)1;
    ret=ledc_timer_config(&ledc_timer);
    if(ESP_OK!=ret){ESP_LOGE(TAG,"Error on ledc_timer_config2:%d",ret);return ret;}
    ledc_channel[0].speed_mode=LEDC_HIGH_SPEED_MODE;
    ledc_channel[1].speed_mode=LEDC_HIGH_SPEED_MODE;
    ledc_channel[0].timer_sel=(ledc_timer_t)0;
    ledc_channel[1].timer_sel=(ledc_timer_t)0;
    ledc_channel[0].channel=(ledc_channel_t)0;

    ledc_channel[1].channel=(ledc_channel_t)1;
    ledc_channel[0].gpio_num=LEDC_CH0_GPIO;
    ledc_channel[1].gpio_num=LEDC_CH1_GPIO;
    ledc_channel[0].duty= deg2pw(degS);
    ledc_channel[1].duty= deg2pw(NeutralA);
    
    for (int ch = 0; ch <2; ch++) {
        ret=ledc_channel_config(& (ledc_channel[ch]));
        if(ESP_OK != ret){ESP_LOGE(TAG,"Error on ledc_channel:%d",ret);return ret;}
    }


    ydlidar_begin(UART_NUM_2,17,16,NULL,NULL);

    if(ESP_OK==ret)xTaskCreatePinnedToCore(servoTask, "servoTask", STACK_SIZE, NULL, 1, &servoTaskHandle, 0);

    return ret;
}


extern "C"{
esp_err_t servoTaskSetup(void){return _servoTaskSetup();}
struct sockaddr_in remote_addr;
void servoCommand(int mysocket){
    static char databuff[100];
    size_t socklen = sizeof(remote_addr);
    
    int len = recvfrom(mysocket, databuff, sizeof(databuff)-1, 0, (struct sockaddr *)&remote_addr, &socklen);
    if (len > 0)
    {
        databuff[len]=0;
        
        float a,s;
        if(2==sscanf(databuff,"%f %f",&a,&s)){
            lockServo();
            targetSpeed=a;
            degS=s;
            udpWdtCount=5;
            memcpy(&host_addr,&remote_addr,sizeof(host_addr));
            _mysocket=mysocket;
            unlockServo();
        }

        ///sprintf(databuff,"%d %d",ledc_channel[0].duty,ledc_channel[1].duty);
        //ESP_LOGI(TAG,"Now %s",databuff);

        //just echo
        //remote_addr.sin_port = htons(13532);
        //ESP_LOGI(TAG, "data come from %s", inet_ntoa(remote_addr.sin_addr.s_addr));
        //sendto(mysocket, databuff, strlen(databuff)+1, 0, (struct sockaddr *)&remote_addr, sizeof(remote_addr));
    }

}
}