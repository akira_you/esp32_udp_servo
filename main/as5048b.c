#include "as5048b.h"
#include "esp_log.h"
static const char *TAG = "AS5048B";
#define AS5048_ADDRESS 0x40 // 0b10000 + ( A1 & A2 to GND)
#define AS5048B_DIAG_REG 0xFB

esp_err_t as5048b_setup(as5048b *h,uint8_t additional_addr)
{
    h->addr=AS5048_ADDRESS + additional_addr;
    esp_err_t ret;
    uint8_t buf[2];
    buf[0]=buf[1]=0;

    ret=i2c_write_reg(h->addr,0x16,buf,2); //reset zero reg
    if(ret!=ESP_OK){
        ESP_LOGE(TAG,"readError on setup0:%d",ret);
        return ret;
    }



    ret=i2c_read_reg(h->addr,AS5048B_DIAG_REG,buf,1);
    if(ret!=ESP_OK){
        ESP_LOGE(TAG,"readError on setup1:%d",ret);
        return ret;
    }
    ESP_LOGI(TAG,"Diag %x",(int)(buf[0]));
    ret=i2c_read_reg(h->addr,0xff,buf,1);
    if(ret!=ESP_OK){
        ESP_LOGE(TAG,"readError on setup2:%d",ret);
        return ret;
    }
    h->_lastPos=buf[0];
    h->nowPos=0;
    return ret;
}

esp_err_t as5048b_kick(as5048b *h, xSemaphoreHandle *angleSem)
{
    esp_err_t ret;
    uint8_t buf[1];
    ret=i2c_read_reg(h->addr,0xff,buf,1);
    if(ret!=ESP_OK){
        ESP_LOGE(TAG,"readError on kick:%d",ret);
        return ret;
    }
    xSemaphoreTake(*angleSem, portMAX_DELAY ) ;
    int8_t diff=buf[0]-h->_lastPos;
    h->nowPos+=diff;
    h->_lastPos=buf[0];
    xSemaphoreGive(*angleSem);

    return ret;
}