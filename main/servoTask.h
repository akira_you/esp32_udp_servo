#ifndef _SERVO_TASK_H_
#define _SERVO_TASK_H_
#include "freertos/FreeRTOS.h"
#include "angleTask.h"
#ifdef __cplusplus
extern "C"{
#endif 
esp_err_t servoTaskSetup(void);
void servoCommand(int mysocket);
#ifdef __cplusplus
}
#endif
#endif