#ifndef _ANGLE_TASK_H_
#define _ANGLE_TASK_H_
#include "freertos/FreeRTOS.h"

#ifdef __cplusplus
int16_t getAngle(); //only in c++
uint8_t _getAangleRaw(); //only in c++
extern "C"{
#endif 
esp_err_t angleTaskSetup(void);
#ifdef __cplusplus
}
#endif 


#endif
