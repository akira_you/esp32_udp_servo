#ifndef AS5048B_H
#define AS5048B_H
#include "i2c.h"
#ifdef __cplusplus
extern "C"{
#endif

typedef struct {
    uint8_t addr;
    uint8_t _lastPos;
    int16_t nowPos;
} as5048b;
esp_err_t as5048b_setup(as5048b *h,uint8_t additional_addr);
esp_err_t as5048b_kick(as5048b *h, xSemaphoreHandle *angleSem);

#ifdef __cplusplus
}
#endif

#endif