#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
const static char *TAG="angleTask";
#define STACK_SIZE 3000
#include "esp_log.h"

/////////////////////////
//task for Angle Sensor
///////////////////////
xTaskHandle angleTaskHandle;

#include "as5048b.h"
as5048b angleHandle;
xSemaphoreHandle angleSem = NULL;
int16_t getAngle(){
    int16_t ret;
    xSemaphoreTake(angleSem, portMAX_DELAY ) ;
    ret=angleHandle.nowPos;
    xSemaphoreGive(angleSem);
    return ret;
}
uint8_t _getAangleRaw(){
    int8_t ret;
    xSemaphoreTake(angleSem, portMAX_DELAY ) ;
    ret=angleHandle._lastPos;
    xSemaphoreGive(angleSem);
    return ret;
}




void angleTask(void *pvParameters)
{
    while(true){
        as5048b_kick(&angleHandle, &angleSem);
        vTaskDelay(10 / portTICK_PERIOD_MS);  //Max rotaion is CommandLoop speed /2
    }   
}
esp_err_t _angleTaskSetup(void){
    ESP_LOGI(TAG,"angleTaskSetup start");
    angleSem=xSemaphoreCreateMutex();
    esp_err_t ret;
    ret=i2c_setup_master();
    if(ret!=ESP_OK){ESP_LOGE(TAG,"onAngleTaskSetup1:%d",ret);return ret;}
    ret=as5048b_setup(&angleHandle,0);
    if(ret!=ESP_OK){ESP_LOGE(TAG,"onAngleTaskSetup2:%d",ret);return ret;}
    xTaskCreatePinnedToCore(angleTask, "angleTask", STACK_SIZE, NULL, 1, &angleTaskHandle, 0);
    return ret;
}


extern "C"{
esp_err_t angleTaskSetup(void){return _angleTaskSetup();}
}