#include <string.h>
#include <time.h>
#include <sys/time.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "esp_attr.h"
#include "esp_sleep.h"
#include "nvs_flash.h"
#include "lwip/err.h"
#include <string.h>
#include <sys/socket.h>
#include <math.h>
#include "angleTask.h"
#include "servoTask.h"
static const char *TAG = "UDP_SERVO";


#define STACK_SIZE 3000


static void initialise_wifi(void);
static esp_err_t event_handler(void *ctx, system_event_t *event);
const uint16_t port = 13531;
int get_socket_error_code(int socket);
int show_socket_error_reason(const char *str, int socket);
static int mysocket;
struct sockaddr_in local_addr;


//Loop while Wifi connected
//Command recv on 13531/UDP and ack on port 1352/UDP
static void CommandLoop(void)
{
    servoCommand(mysocket);
    vTaskDelay(10 / portTICK_PERIOD_MS);
}
//setup after connecting WiFi
static void setupAfterWiFi(void){

    ESP_ERROR_CHECK(servoTaskSetup());
    ESP_LOGI(TAG, "setupAfterWiFi done!");
}
//setup before connecting WiFi (IO reset)
static void setupBeforeWiFi(void)
{
    ESP_ERROR_CHECK(angleTaskSetup());
    ESP_LOGI(TAG, "setupBeforeWiFi done!");
}










////////////////////////////////////////
//// Fllowings are utility functions
///////////////////////////////////////
const int NOF_SSIDS=3;
const char* WIFI_SSID[]={ CONFIG_WIFI_SSID3,CONFIG_WIFI_SSID1,CONFIG_WIFI_SSID2};
const char* WIFI_PASS[]={ CONFIG_WIFI_PASSWORD3,CONFIG_WIFI_PASSWORD1,CONFIG_WIFI_PASSWORD2};
/* FreeRTOS event group to signal when we are connected & ready to make a request */
static EventGroupHandle_t wifi_event_group;
/* The event group allows multiple bits for each event,
   but we only care about one event - are we connected
   to the AP with an IP? */
const int CONNECTED_BIT = BIT0;
/* Variable holding number of times ESP32 restarted since first boot.
 * It is placed into RTC memory using RTC_DATA_ATTR and
 * maintains its value when ESP32 wakes from deep sleep.
 */
RTC_DATA_ATTR static int8_t boot_count;


//setupBeforeWiFi after Get IP (socket connection)
static esp_err_t setupWithIP(void)
{
    mysocket = socket(AF_INET, SOCK_DGRAM, 0);
    if (mysocket < 0)
    {
        ESP_LOGE(TAG, "... faild to socket");
        show_socket_error_reason(TAG, mysocket);
        return ESP_FAIL;
    }
    local_addr.sin_family = AF_INET;
    local_addr.sin_port = htons(port);
    local_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(mysocket, (struct sockaddr *)&local_addr, sizeof(local_addr)) < 0)
    {
        show_socket_error_reason(TAG, mysocket);
        close(mysocket);
        ESP_LOGE(TAG, "... faild to bind");
        return ESP_FAIL;
    }

    struct timeval receiving_timeout;
    receiving_timeout.tv_sec = 1;
    receiving_timeout.tv_usec = 0;
    if (setsockopt(mysocket, SOL_SOCKET, SO_RCVTIMEO, &receiving_timeout, sizeof(receiving_timeout)) < 0)
    {
        ESP_LOGE(TAG, "... failed to set socket receiving timeout");
        return ESP_FAIL;
    }
    return ESP_OK;
}
void app_main()
{
    ++boot_count;
    nvs_flash_init();
    ESP_LOGI(TAG, "Boot count: %d", boot_count);
    initialise_wifi();
    vTaskDelay(100 / portTICK_PERIOD_MS); 
    setupBeforeWiFi();
    vTaskDelay(100 / portTICK_PERIOD_MS); 
    
    xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT,
                        false, true, portMAX_DELAY);
    if (ESP_OK == setupWithIP())
    {
        setupAfterWiFi();
        //vTaskStartScheduler();   
        while (xEventGroupGetBits(wifi_event_group) & CONNECTED_BIT)
            CommandLoop();
    }
    ESP_ERROR_CHECK(esp_wifi_stop());
    //esp_restart();
    esp_deep_sleep(1000000LL);

    //esp_deep_sleep(1000000LL * deep_sleep_sec);
}

static uint32_t wifi_get_local_ip(void)
{
    int bits = xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT, 0, 1, 0);
    tcpip_adapter_if_t ifx = TCPIP_ADAPTER_IF_AP;
    tcpip_adapter_ip_info_t ip_info;
    wifi_mode_t mode;

    esp_wifi_get_mode(&mode);
    if (WIFI_MODE_STA == mode)
    {
        bits = xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT, 0, 1, 0);
        if (bits & CONNECTED_BIT)
        {
            ifx = TCPIP_ADAPTER_IF_STA;
        }
        else
        {
            ESP_LOGE(TAG, "sta has no IP");
            return 0;
        }
    }

    tcpip_adapter_get_ip_info(ifx, &ip_info);
    return ip_info.ip.addr;
}
/*
static uint32_t wifi_get_broadcast_ip(void)
{
    int bits = xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT, 0, 1, 0);
    tcpip_adapter_if_t ifx = TCPIP_ADAPTER_IF_AP;
    tcpip_adapter_ip_info_t ip_info;
    wifi_mode_t mode;

    esp_wifi_get_mode(&mode);
    if (WIFI_MODE_STA == mode)
    {
        bits = xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT, 0, 1, 0);
        if (bits & CONNECTED_BIT)
        {
            ifx = TCPIP_ADAPTER_IF_STA;
        }
        else
        {
            ESP_LOGE(TAG, "sta has no IP");
            return 0;
        }
    }

    tcpip_adapter_get_ip_info(ifx, &ip_info);
    return ip_info.ip.addr | (~ip_info.netmask.addr);
}
*/
static void initialise_wifi(void)
{
    //nvs_flash_init();
    tcpip_adapter_init();
    wifi_event_group = xEventGroupCreate();
    ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL));
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
    wifi_config_t wifi_config;
    bzero(&wifi_config,sizeof(wifi_config));
    
    strcpy((char*) wifi_config.sta.ssid,    WIFI_SSID[boot_count%NOF_SSIDS]);
    strcpy((char*) wifi_config.sta.password,WIFI_PASS[boot_count%NOF_SSIDS]);

    ESP_LOGI(TAG, "Setting WiFi configuration SSID %s...%s", wifi_config.sta.ssid,wifi_config.sta.password);
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
    ESP_LOGI(TAG, "wifi config");
    ESP_ERROR_CHECK(esp_wifi_start());
    ESP_LOGI(TAG, "wifi_start");
    
}

static esp_err_t event_handler(void *ctx, system_event_t *event)
{
    esp_err_t ret;
    switch (event->event_id)
    {
    case SYSTEM_EVENT_STA_START:
        if(ESP_OK!=(ret=esp_wifi_connect())){
            ESP_LOGE(TAG,"esp_wifi_connect error: %d",ret);
            //sleep(500);
            vTaskDelay(100 / portTICK_PERIOD_MS); 
            esp_deep_sleep(1000000LL);
        }
        ESP_LOGI(TAG,"WiFi started");
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
        {
            wifi_ap_record_t wifidata;
            uint32_t ip = wifi_get_local_ip();
            if (esp_wifi_sta_get_ap_info(&wifidata)==0)
            {
            ESP_LOGI(TAG,"************** rssi:%d\r\n", wifidata.rssi);
            }
            char *ipstr = inet_ntoa(ip);
            ESP_LOGW(TAG, "IP:%s", ipstr);
            xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
        }
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
            ESP_LOGE(TAG,"WiFi Disconnected");
        /* This is a workaround as ESP32 WiFi libs don't currently
           auto-reassociate. */
        //sleep(500);
        vTaskDelay(100 / portTICK_PERIOD_MS); 
        esp_deep_sleep(1000000LL);
        //esp_wifi_connect();
        //xEventGroupClearBits(wifi_event_group, CONNECTED_BIT);
        break;
    default:
       ESP_LOGE(TAG,"WiFi not suppoeted event:%d",event->event_id);
        break;
    }
    return ESP_OK;
}
int get_socket_error_code(int socket)
{
    int result;
    u32_t optlen = sizeof(int);
    int err = getsockopt(socket, SOL_SOCKET, SO_ERROR, &result, &optlen);
    if (err == -1)
    {
        ESP_LOGE(TAG, "getsockopt failed:%s", strerror(err));
        return -1;
    }
    return result;
}
int show_socket_error_reason(const char *str, int socket)
{
    int err = get_socket_error_code(socket);

    if (err != 0)
    {
        ESP_LOGW(TAG, "%s socket error %d %s", str, err, strerror(err));
    }

    return err;
}
